# Schemeparser-rs

A scheme based config serializator. Dynamically configures the scheme to read a scheme based config files and serialize the parsed data into JSON formatted message ready to be directly deserialized with serde.

Previously this lib used rusty_scheme-rs, but the scheme lexer was rewritten to reduce the dependencies.

todo:  
- replace panic! with SchemeResult