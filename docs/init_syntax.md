# Syntax

The output of the schema is serialized into JSON, so it could be deserialized directly into Rust struct or maybe C-struct, or parsed.

`;` - comment  
`quote`, `unquote`, `quasiquote` - a predefined identifiers and can not be used as an symbols or identifiers.  
`0u` or `0U` - is for unsigned integer. Integers are 64-bit length.  
`'(...)` - is for list.
`#t` and `#f` - are **true** or **false**, used for boolean.

## Data converions and operations manual
 The first part which starts from `(arg...)` tells the parser how to read the arguments provided after the procedure's symbol. The second part consists from `(proc...)` which tells which procedures may be included in the current one. Both are optional, but empty procedures can not exists.

`(arg...)` are readed in order which was set in the script i.e
```lisp
(arg ...)
(arg ...)
```
`(proc...)` can be set in arbitrary order

During the serialization process the `(arg...)` is working with primitives (simple data types like i64, u64, vec) which allowes be casted into other types or in case of the **vec** which allows to cast the content to other type. Casting is unsafe operation and may cause troubles.

Datatype table:
|arg|serialization|
|---|----|
|string|f/string|
|integer|f/integer|
|uinteger|f/uinteger|
|boolean|f/boolean|
|strsymbol|f/string|
|intsymbol|f/integer|
|vec[arg]|f/list[arg]|

Conversion table:
<table>
  <tr>
    <th>From/To</th>
    <th>f/string</th>
    <th>f/integer</th>
    <th>f/uinteger</th>
    <th>f/boolean</th>
  </tr>
  <tr>
    <th>string</th>
    <td>Native</td>
    <td>Ok, Digits</td>
    <td>!May, Digits</td>
    <td>!May, require true/false match</td>
  </tr>
  <tr>
    <th>integer</th>
    <td>Ok</td>
    <td>Native</td>
    <td>Ok, but sign conv</td>
    <td>Ok</td>
  </tr>
  <tr>
    <th>uinteger</th>
    <td>Ok</td>
    <td>Ok, but sign conv</td>
    <td>Native</td>
    <td>Ok</td>
  </tr>
  <tr>
    <th>boolean</th>
    <td>Ok</td>
    <td>Ok</td>
    <td>Ok</td>
    <td>Native</td>
  </tr>
  <tr>
    <th>strsymbol</th>
    <td>Native</td>
    <td>!May fail</td>
    <td>!May fail</td>
    <td>!May fail</td>
  </tr>
  <tr>
    <th>intsymbol</th>
    <td>Ok</td>
    <td>Native</td>
    <td>Ok, sign conv</td>
    <td>Ok, may be wrong interpr</td>
  </tr>
</table>

Required data for successfull converion:
<table>
  <tr>
    <th>From/To</th>
    <th>f/string</th>
    <th>f/integer</th>
    <th>f/uinteger</th>
    <th>f/boolean</th>
  </tr>
  <tr>
    <th>string</th>
    <td>Native</td>
    <td>0-9|+-</td>
    <td>0-9</td>
    <td>True/False, ASCII only</td>
  </tr>
  <tr>
    <th>integer</th>
    <td>Any</td>
    <td>Native</td>
    <td>signed -> unsigned</td>
    <td>(0<=) False (1 <=) True</td>
  </tr>
  <tr>
    <th>uinteger</th>
    <td>Any</td>
    <td>unsigned -> signed</td>
    <td>Native</td>
    <td>(0==) False (1 <=) True</td>
  </tr>
  <tr>
    <th>boolean</th>
    <td>Any</td>
    <td>Any</td>
    <td>Any</td>
    <td>Native</td>
  </tr>
  <tr>
    <th>strsymbol</th>
    <td>Native</td>
    <td>0-9|+-</td>
    <td>0-9</td>
    <td>True/False, ASCII only</td>
  </tr>
  <tr>
    <th>intsymbol</th>
    <td>Any</td>
    <td>Native</td>
    <td>signed -> unsigned</td>
    <td>(0<=) False (1 <=) True</td>
  </tr>
</table>

The converion should be used only when it is really necessary.  
The bitwidth of the integers are 64-bit.  

## (serializator)
Declares a new schema structure for serialization of the contents of the file. Requires a literal string as argument which is assigned to schema which will be used to retrive the schema.

```lisp
(serializator "formats"
    ...contents...
)
```

## (procedure)
Declares an identifier which will be defined as procedure and how the arguemnts and the inline procedures should be read.  

> procedure [title:string] [empty:optional_symbol] (arg) (proc)

`empty` - the symbol `p/empty` can be set when it is expected that the procedure will be empty.  
`title` - is an identifier name for the procedure.  
`(arg)` - arguments, must be set in order you expect to read them in schema.  
`(proc)` - embedded procedures, can be set in different order.

```lisp
    (procedure "override"
        (arg string "overkey" ...)
        (arg string "overval" ...)
        (proc '("action") ...)
    )

    ; for the following schema
    (override "abc" "def")
    (override "foo" "bar"
        (action ...)
    )
```

Example with empty procedure:
```lisp
(serializator "test"

    (procedure "none" p/empty
        ;; todo
    )

    (procedure "path"
        (arg string "pathname"
            (field/as 0u "pathname" (dt f/string))
        )
    )

    (procedure "sqlite"
        (proc '("path")
            (field/as 0u "pathname" (dt f/field "pathname"))
        )
    ) 

    (procedure "database"
        (proc '("none" "sqlite")
            (field/as 0u "databasetype" 
                (dt f/enum
                    (enum/as "none" "None")
                    (enum/as "sqlite" "Sqlite")
                )
            )
        )
    )
    
    (rootprocedure "commandstruct"
        (proc '("database")
            (field/as 0u "database" (dt f/field "databasetype"))
            ; database: Option<EnumDatabase>
        )
    )
)
```

## (arg)
Declares argument for the procedure.
> arg [datatype:symbol] [tag:string]

The datatype is a symbol which declares the type of the data:
- string
- integer
- uinteger (requires U or u after the digit)
- boolean
- vector [sybtype:symbol] i.e string

The `tag` is used for debug and error output purposes.

Can not be `optional` or `repeatable`.

## (proc)
Declares embedded procedures for the procedure.

> proc [procedure_title:list<string>]

`procedure_title` - an identifier names of the (procedure) which could be found in current instance. Usually, only one identifier is set, but if **enum** is expected then more than one identifier can be set.   
The flags is a symbols which modificates behaviour:quasiquote
  
```lisp
    (procedure "ruleset"
        (arg string "rulesetname" ...)
        (proc "action" ...) 
        (proc '("all" "any" "not") ...) 
    )

    ; for the following schema
    (ruleset "ruleset1"
        (action ...)
        (all ...)
    )
```

## (field/as)
Declares how each the `arg` or `proc` is serialized. This includes: field order, datatype, fieldname.  

> field/as  
> [order:uinteger]  
> [[fieldname:string] | [(anon):symbol]]  
> [(dt):procedure]

`order` is unsigned integer which defines the order of the field for data in the structure. It is not necessary that it should be set in strict sequence, but the count must be sequiential without gaps, for example 0, 1,3,2,4... but not 0,1,2,4,5 where 3 is missing. 
`fieldname` - is to specify the fieldname matching name of the field in deserializable structure. If there is no fieldname, then use symbol `anon`.  
`(dt)` - a procedure which describes how to store the data in the field

## (dt)
Tells the serializator how to store the data.

> dt [datatype:symbol] [optional declarations]

`datatype` is a symbol which identifies the dataype:
- `f/string` a (primitive) single string
- `f/integer` `f/uinteger` a (primitive) are for integer values
- `f/boolean` a (primitive) is for boolean
- `f/list` (both primitive and advanced) is for vector (sequential)
- `f/hashmap` is for hashmap (linkedhashmap - sequential)
- `f/struct` is for struct
- `f/enum` is for enumerator
- `f/field` is take a foregn field from a child struct and store in the current struct



### [optional declarations] extra declarations
For the `f/string` `f/integer` `f/uinteger` `f/boolean` in case of
- **arg** - does not require extra 
- **proc** - not possible

For example:
```lisp
(procedure "severity"
    (arg uinteger "sevlvl"
        (field/as 0u "sevvalue" (dt f/uinteger))
    )
)

(procedure "descr"
    (arg string "description"
        (field/as 0u "descrvalue" (dt f/string))
    )
)

; this procedure stores args from procs: severity and descr as own field
; but not as structure
(procedure "regex"
    (arg string "data"
        (field/as 0u f/string "regexstr")
    )
    (proc '("severity")
        (flags optional)
        (field/as 2u "sev" (dt f/field "sevvalue")) ; severity.sevvalue
    )
    (proc '("descr")
        (flags optional)
        (field/as 1u "descr" (dt f/field "descrvalue")) ; descr.descrvalue
    )
)
```

For the `f/field` in case of
- **arg** not possible
- **proc** requires foregn field name [fieldname:string]

---
**NOTE**

See example above.

---


For the `f/list` in case of
- **arg** requires a **primitive** datatype to which the data will be converted before serialization. @See *data_converions*
- **proc** requires an advanced datatype like `f/struct`, `f/enum`, `f/field`. Nested lists are not supported.  
  
For example:
```lisp
; arg is Vector of 'some' strsymbols
; f/list has arbitrary 'value'-arg name, can be self for debug
(procedure "privcommands"
    (arg vector "command" strsymbol 
        (field/as 0u "cmd" (dt f/list f/string)) ; f/string because strsymbol
        ; but by setting f/integer, it will try to convert strsymbol to i64
    )
)

; 'some' procedure has (arg) cmdlist, which is stored as
; local field
(procedure "privcommands"
    (proc '("command")
        (field/as 0u "cmd" (dt f/list f/field "cmdlist"))
        ; list stores field from procedure command
    )
)

; example for enumerator
proc '("all" "any" "not")
    (flags repeat optional)
    (field/as 2u "rules" 
        (dt f/list f/enum
            (enum/as "all" "All")
            (enum/as "any" "Any")
            (enum/as "not" "Not")
        )
    )
) 
```
For the `f/hashmap` in case of
- **arg** impossible type, will produce error
- **proc** equires a **foreign field name** for the key, which always a `f/string` an advanced datatype like `f/list`, `f/struct`, `f/enum`, `f/field`. Nested hashmaps are not supported.

```lisp
    (proc '("action") 
        (flags repeat optional)
        (field/as 1u "actions" (dt f/hashmap "actiontitle" f/struct))
    ) 
```

For the `f/struct` in case of
- **arg** impossible type, will produce error
- **proc** does not require any additional declarations

---
**NOTE**

See examples above.

---

For the `f/enum` in case of
- **arg** impossible type, will produce error
- **proc** does not require additonal declarations but require to include an alias map `enum/as`.  
The enum can be anonymous. It mean that it is not serialized with `fieldname`, but with its internal naming. It is used for nesting the enums or in some other cases.

For example:
```lisp
(procedure "ruleset"
    ...
    ; there, it is stored "all" "any" "not" with fieldname "rules"
    ; it will looklike i.e rules:{All:... Any:...}
    (proc '("all" "any" "not")
        (flags repeat optional)
        (field/as 2u "rules" 
            (dt f/list f/enum
                (enum/as "all" "All")
                (enum/as "any" "Any")
                (enum/as "not" "Not")
            )
        )
    ) 
    ...
)

(procedure "all"
    ; there we store ... without fieldname
    ; it will lookelike i.e All:<data>
    (proc '("any" "not" "all" "regex" "literal")
        (flags repeat optional)
        (field/as 0u anon  ; anonymous enum
            (dt f/list f/enum 
                (enum/as "all" "All")
                (enum/as "any" "Any")
                (enum/as "not" "Not")
                (enum/as "regex" "Regex")
                (enum/as "literal" "Literal")
            )
        )
    ) 
)
```

### (flags)
Every **proc** may be modified with the following modificators:
- `repeat` is alloved to appear more than once
- `optional` may not appear, also tells serializator to treat the field as Optional<> or NULL

The datatypes: `f/list`, `f/hashmap` requires that the **proc** is set with the flag `repeat`, otherwise it will procuce a runtime error.

If it is not required to set any flags, simply don't declare (flags).  
The **arg** can not be modified by any modifiers.

### (enum/as)
For the serialization into `f/enum`, the name converion must be used. It converts the title of the identifier **proc** into the required name AND it declares which **enum** elements are allowed in current procedure.

```lisp
(procedure "ruleset"
    ...
    (proc '("all" "any" "not")
        (flags repeat optional)
        (field/as 2u "rules" 
            (dt f/list f/enum 
                (enum/as "all" "All")
                (enum/as "any" "Any")
                (enum/as "not" "Not")
            )
        )
    ) 
)
...

(procedure "all"
    (proc '("any" "not" "all" "regex" "literal")
        (flags repeat optional)
        (field/as 0u anon 
            (dt f/list f/enum
                (enum/as "all" "All")
                (enum/as "any" "Any")
                (enum/as "not" "Not")
                (enum/as "regex" "Regex")
                (enum/as "literal" "Literal")
            )
        )
    ) 
)

```

## (define)

Defines the symbol and its procedure space. Used when schema uses hardcoded symbols.

> (define [title:string] [data:string|integer|boolean] [namespaces:Vector(string):optional])

`title` is identifier of the symbol.
`data` is what it represents. It can be string or integer or boolean.
`namespaces` is to which procedures the symbol should limited. If not declared then it can be used in all procedures.

For example, defined schema:
```lisp
(serializator "test"
    (define "move" "mv" '("commands"))
    (define "copy" "cp" '("commands"))
    (define "remove" "rm" '("privcommands"))
    
    (procedure "privcommands"
        (arg vector "command" strsymbol 
            (field/as 0u "cmd" (dt f/list f/string))
        )
    )

    (procedure "commands"
        (arg vector "command" strsymbol 
            (field/as 0u "cmd" (dt f/list f/string))
        )
    )

    (rootprocedure "commandstruct"
        (proc '("commands")
            (field/as 0u "cmdstruct" (dt f/struct))
        )

        (proc '("privcommands")
            (field/as 1u "privcmdstruct" (dt f/struct))
        )
    )
)
```
during data serialization process of the following:
```lisp
(commands '(move remove)) ; must fail
(privcommands '(move remove)) ; must fail too, but never will be reached
```

will throw a runtime error, because *remove* can not be used with procedure *commands*.

## rootprocedure
Must be defined in order to tell scheme parser which procedures are in the root of the document.

Same as (procedure) but name can be arbitrary and it can not have arguments.

# Examples

```lisp
(interpritator "test"
;; ! this is partial example missing (field/enum)
    (procedure "override"
        (arg string "overkey")
        (arg string "overval")
    )

    (procedure "ruleset"
        (arg string "rulesetname")
        (proc "action" repeat optional) 
        (proc "all" repeat optional) 
        (proc "any" repeat optional) 
        (proc "not" repeat optional)
        
    )

    (procedure "action"
        (arg string "actionname")
        (proc "override" repeat optional)
    )

    (procedure "all"
        (proc "any" repeat optional) 
        (proc "not" repeat optional) 
        (proc "all" repeat optional) 
        (proc "regex" repeat optional) 
        (proc "literal" repeat optional)
    )

    (procedure "any"
        (proc "any" repeat optional) 
        (proc "not" repeat optional) 
        (proc "all" repeat optional) 
        (proc "regex" repeat optional) 
        (proc "literal" repeat optional)
    )

    (procedure "not"
        (proc "any" repeat optional) 
        (proc "not" repeat optional) 
        (proc "all" repeat optional) 
        (proc "regex" repeat optional) 
        (proc "literal" repeat optional)
    )

    (procedure "severity"
        (arg uinteger "sevlvl")
    )

    (procedure "descr"
        (arg string "description")
    )

    (procedure "regex"
        (arg string "data")
        (proc "severity" optional)
        (proc "descr" optional)
    )

    (procedure "literal"
        (arg string "data")
        (proc "severity" optional)
        (proc "descr" optional)
    )

    (rootprocedure "filterstruct"
        (proc "ruleset" repeat)
    )
)
```


## other

When **proc** has set flag `optional` then in the Rust struct, the field must be Optional<>.  
When the **proc** flag `repeat` is set for the non-repeatable datatypes like `f/integer` it will trigger the error as duplicate with repeatable flag.  
When the **proc** is serialized as `f/struct` then it will be stored as a structure.   
When the field is serialized as enum, `enum` can be anonymous. It is used for embedded enums.

The struct is serialized as vector i.e vec![item, item, item].  
ToDo...