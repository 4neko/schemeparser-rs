

pub fn contains_printable_any(st: &String) -> bool
{
	return st.chars().any(|x| char::is_alphanumeric(x) | x.is_ascii_punctuation() | x.is_ascii_graphic());
}

pub fn contains_printable_all(st: &str) -> bool
{
	return st.chars().all(|x| char::is_alphanumeric(x) | x.is_ascii_punctuation() | x.is_ascii_graphic());
}

pub fn contains_printable_vec_all(st: &Vec<String>) -> bool
{
    for s in st.iter()
    {
        if s.chars().all(|x| char::is_alphanumeric(x) | x.is_ascii_punctuation() | x.is_ascii_graphic()) == false
        {
            return false;
        }
    }

	return true;
}


/// remove whitespaces
pub fn remove_whitespaces(s: String) -> String
{
	let tmp: String = s.chars().filter(|c| !c.is_whitespace()).collect();

	return tmp;
}
