
/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use linked_hash_map::LinkedHashMap;

use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError};
use crate::scheme_runtime_error;

use super::serialization::ProcElements;
use super::schemesubs::SerializeAs;
use super::dynamic_serialize::SerDynValue;
use super::schemesubs::SchemeProc;

pub fn store_single_enum(alias: &LinkedHashMap<String, String>,
                        proc: ProcElements,
                        osi: &mut Option<(String, SerDynValue)>,
                        proc_struct: &SchemeProc) -> SchemeResult<()>
{
    
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if osi.is_some() == true
    {
        //already present, it is not list, so throw error
        if flags.is_repeatable() == false
        {
            scheme_runtime_error!("storing single enum failed: Duplicate insert for proc: '{}', for: '{}'", 
                                    proc.get_proc_name_str(), serlz);
        }
        else
        {
            scheme_runtime_error!("storing single enum failed: repeatable but, duplicate insert for proc: '{}', for: '{}'", 
                                    proc.get_proc_name_str(), serlz);
        }
    }

    //convert procname to alias if available
    let pa = match alias.get(proc.get_proc_name_str())
    {
        Some(r) => r.clone(),
        None => 
            scheme_runtime_error!("storing single enum failed: enum/as missing for {} or not suppoed to be there? for: '{}'", 
                                    proc.get_proc_name_str(), serlz)
    };

    let fkd = store_enum(proc);

    if serlz.is_anonymous() == true
    {
        *osi = Some((pa, fkd));
    }
    else
    {
        let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
        k.insert(pa, fkd);
        *osi = Some((serlz.clone_field_name(), SerDynValue::Hashmap(k)));
    }

    return Ok(());
}

pub fn store_list_enum(alias: &LinkedHashMap<String, String>,
                        proc: ProcElements,
                        osi: &mut Option<(String, SerDynValue)>,
                        proc_struct: &SchemeProc) -> SchemeResult<()>
{
    
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    //convert procname to alias if available
    let pa = match alias.get(proc.get_proc_name_str())
    {
        Some(r) => r.clone(),
        None => proc.get_proc_name_str().clone()
    };

    // extract the struct and store it as list struct -> [, ,] or single arg
    let fkd = store_enum(proc);
    
    //push to the list, so = [{"Dcd":[[],8]},{"Dcd":[[],9]}]
    if osi.is_some() == true
    {
        //already exists
        if serlz.is_anonymous() == true
        {
            let (_, ref mut sdv_list) = osi.as_mut().unwrap();
            let list = sdv_list.unwrap_list();

            let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
            k.insert(pa.clone(), fkd);

            list.push(SerDynValue::Hashmap(k));
        }
        else
        {
            let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
            k.insert(pa.clone(), fkd);
            
            let (_, ref mut sdv_list) = osi.as_mut().unwrap();

            let list = sdv_list.unwrap_list();
            list.push(SerDynValue::Hashmap(k));
        }
    }
    else
    {
        if serlz.is_anonymous() == true
        {
            //first time added
            //{"Dcd":[[],8]} -> [{"Dcd":[[],8]},]
            
            let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
            k.insert(pa.clone(), fkd);
            let prep = SerDynValue::List(vec![SerDynValue::Hashmap(k)]);

            //if anonymous then don't add field name {"Abc":[{"Dcd":[[],8]},{"Dcd":[[],9]}]}
            *osi = Some((pa, prep));//prep));
        }
        else
        {
            // turn it into enum key:val = {"Dcd":[[],8]}
            let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
            k.insert(pa.clone(), fkd);
            let prep = SerDynValue::List(vec![SerDynValue::Hashmap(k)]);

            //"field4":{"Abc":[{"Dcd":[[],8]},{"Dcd":[[],9]}]}
            *osi = Some((serlz.clone_field_name(), prep));
        }
    }

    return Ok(());
}

pub fn store_hashmap_enum(alias: &LinkedHashMap<String, String>,
                        proc: ProcElements,
                        osi: &mut Option<(String, SerDynValue)>,
                        proc_struct: &SchemeProc,
                        foregndata_key: String) -> SchemeResult<()>
{
    
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if serlz.is_anonymous() == true
    {
        scheme_runtime_error!("store hashmap enum failed: hashmap enum can not be anonymous for proc: '{}' for '{}'", 
                                proc.get_proc_name_str(), serlz);
    }

    //convert procname to alias if available
    let pa = match alias.get(proc.get_proc_name_str())
    {
        Some(r) => r.clone(),
        None => scheme_runtime_error!("store hashmap enum failed: enum/as missing for {} or not suppoed to be there? for '{}'", 
                                        proc.get_proc_name_str(), serlz)
    };

    // extract the struct and store it as list struct -> [, ,] or single arg
    let fkd = store_enum(proc);

    // turn it into enum key:val = {"Dcd":[[],8]}
    let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
    k.insert(pa.clone(), fkd);

    if osi.is_some() == true
    {
        if flags.is_repeatable() == false
        {
            scheme_runtime_error!("store hashmap enum failed: enum {} is not repeatable but repeats, for '{}'",  
                                    pa, serlz);
        }

        let (_s, ref mut val) = osi.as_mut().unwrap();
        let hm = val.unwrap_hashmap();
        let insres = hm.insert(foregndata_key, SerDynValue::Hashmap(k));

        if insres.is_some() == true
        {
            scheme_runtime_error!("store hashmap enum failed: duplicate values in FieldType::HashMap enum: '{}' for: '{}'", 
                                    pa, serlz);
        }
    }
    else
    {
        let mut hm = LinkedHashMap::new();
        hm.insert(foregndata_key, SerDynValue::Hashmap(k));
        
        *osi = Some((serlz.clone_field_name(), SerDynValue::Hashmap(hm)));
    }
    
    return Ok(());
}

fn store_enum(proc: ProcElements) -> SerDynValue //SchemeResult<SerDynValue>
{
    //let serlz = proc_struct.get_proc_serialz();

    let estr = proc.eject_struct();
    
    //extract feilds out of the struct to the list[]
    let fkd = if estr.len() == 1
    {
        let mut itm = SerDynValue::Optional(None);
        let mut set = false;
        
        for (_, v) in estr
        {
            set = true;
            itm = v;
            break;
        }
        if set == false
        {
            panic!("store_enum: estr.len() == 1 but itm was not set");
        }

        //return just single argument 
        itm
    }
    else
    {
        //convert to the list
    
        let mut enumfields: Vec<SerDynValue> = Vec::with_capacity(estr.len());

        for (_, v) in estr
        {
            enumfields.push(v);
        }

        SerDynValue::List(enumfields)
    };
    
    //return Ok(fkd);
    return fkd;
}

pub fn store_list(osi: &mut Option<(String, SerDynValue)>,
                         foregndata: SerDynValue,
                         proc_struct: &SchemeProc) -> SchemeResult<()>
{   
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if osi.is_some() == true
    {
        if flags.is_repeatable() == false
        {
            scheme_runtime_error!("store list failed: is not repeatable but repeats, for '{}'",  
                                    serlz);
        }

        let (_s, ref mut val) = osi.as_mut().unwrap();
        let list = val.unwrap_list();
        list.push(foregndata);
    }
    else
    {
        let fkd = SerDynValue::List(vec![foregndata]);
        *osi = Some((serlz.clone_field_name(), fkd));
    }

    return Ok(());
}

pub fn store_hashmap(osi: &mut Option<(String, SerDynValue)>,
                    foregndata: SerDynValue,
                    proc_struct: &SchemeProc,
                    foregndata_key: String) -> SchemeResult<()>
{
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if osi.is_some() == true
    {
        if flags.is_repeatable() == false
        {
            scheme_runtime_error!("store hashmap failed: is not repeatable but repeats, for '{}'",  
                                    serlz);
        }

        let (_s, ref mut val) = osi.as_mut().unwrap();
        let hm = val.unwrap_hashmap();
        let insres = hm.insert(foregndata_key, foregndata);

        if insres.is_some() == true
        {
            scheme_runtime_error!("store hashmap failed: duplicate values in FieldType::HashMap for: '{}'", 
                                    serlz);
        }
    }
    else
    {
        let mut hm = LinkedHashMap::new();
        hm.insert(foregndata_key, foregndata);

        *osi = Some((serlz.clone_field_name(), SerDynValue::Hashmap(hm)));
    }

    return Ok(());
}

pub fn store_hashmap_list(osi: &mut Option<(String, SerDynValue)>,
                        foregndata: SerDynValue,
                        proc_struct: &SchemeProc,
                        foregndata_key: String) -> SchemeResult<()>
{
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if osi.is_some() == true
    {
        if flags.is_repeatable() == false
        {
            scheme_runtime_error!("store hashmap failed: is not repeatable but repeats, for '{}'",  
                                    serlz);
        }

        let (_s, ref mut val) = osi.as_mut().unwrap();
        let hm = val.unwrap_hashmap();

        if hm.contains_key(&foregndata_key) == true
        {
            let lst = hm.get_mut(&foregndata_key).unwrap();
            let mut list = lst.unwrap_list();
            list.push(foregndata);
        }
        else
        {
            hm.insert(foregndata_key, foregndata);
        }
    }
    else
    {
        let mut hm = LinkedHashMap::new();
        hm.insert(foregndata_key, SerDynValue::List(vec![foregndata]));

        *osi = Some((serlz.clone_field_name(), SerDynValue::Hashmap(hm)));
    }

    return Ok(());
}

pub fn store_hashmap_list_enum(alias: &LinkedHashMap<String, String>,
                                proc: ProcElements,
                                osi: &mut Option<(String, SerDynValue)>,
                                proc_struct: &SchemeProc,
                                foregndata_key: String) -> SchemeResult<()>
{

    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if serlz.is_anonymous() == true
    {
        scheme_runtime_error!("store hashmap list enum failed: hashmap enum can not be anonymous, for '{}' '{}'", 
                                proc.get_proc_name_str(), serlz);
    }

    //convert procname to alias if available
    let pa = match alias.get(proc.get_proc_name_str())
    {
        Some(r) => r.clone(),
        None => scheme_runtime_error!("store hashmap enum failed: enum/as missing for {} or not suppoed to be there? for '{}'", 
                                        proc.get_proc_name_str(), serlz)
    };

    // extract the struct and store it as list struct -> [, ,] or single arg
    let fkd = store_enum(proc);

    // turn it into enum key:val = {"Dcd":[[],8]}
    let mut k: LinkedHashMap<String, SerDynValue> = LinkedHashMap::with_capacity(1);
    k.insert(pa.clone(), fkd);

    return store_hashmap_list(osi, SerDynValue::Hashmap(k), proc_struct, foregndata_key);

    /*if osi.is_some() == true
    {
        if flags.is_repeatable() == false
        {
            panic!("procedure todo is not repeatable but repeats");
        }

        let (_s, ref mut val) = osi.as_mut().unwrap();
        let hm = val.unwrap_hashmap();
        let insres = hm.insert(foregndata_key, SerDynValue::Hashmap(k));

        if insres.is_some() == true
        {
            panic!("todo: duplicate values in FieldType::HashMap enum tp struct");
        }
    }
    else
    {
        let mut hm = LinkedHashMap::new();
        hm.insert(foregndata_key, SerDynValue::Hashmap(SerDynValue::List(vec![k])));

        *osi = Some((serlz.clone_field_name(), SerDynValue::Hashmap(hm)));
    }

    return Ok(());*/
}


pub fn store_field(osi: &mut Option<(String, SerDynValue)>,
                        foregndata: SerDynValue,
                        proc_struct: &SchemeProc) -> SchemeResult<()>
{
    let flags = proc_struct.get_proc_flags();
    let serlz = proc_struct.get_proc_serialz();

    if osi.is_some() == true
    {
        //already present, it is not list, so throw error
        if flags.is_repeatable() == false
        {
            scheme_runtime_error!("store field failed: Duplicate insert for {} {:?}", 
                                    serlz, foregndata);
        }
        else
        {
            scheme_runtime_error!("store field failed: Repeatable but, Duplicate insert for {} {:?}", 
                                    serlz, foregndata);
        }
    }
    else
    {
        *osi = Some((serlz.clone_field_name(), foregndata));
    }

    return Ok(());
}



pub fn get_foregn_key_data<'a>(forkeyname: &'a String, 
                                proc: &ProcElements) -> SchemeResult<(SerDynValue, &'a String)>
{
    let foregndata = match proc.clone_from_struct(forkeyname)
    {
        Some(r) => r,
        None => 
            scheme_runtime_error!("Foreng key does not exist in '{}', for proc: '{}'", 
                                    forkeyname, proc.get_proc_name_str()),
    };

    return Ok((foregndata, forkeyname));
}


pub fn int2bool(i: i64) -> bool
{
    if i == 0
    {
        return false;
    }
    else
    {
        return true;
    };
}

pub fn uint2bool(u: u64) -> bool
{
    if u == 0
    {
        return false;
    }
    else
    {
        return true;
    };
}

pub fn string2boolean(b: &str) -> SchemeResult<bool>
{
    let mut newstr = String::new();

    for c in b.to_lowercase().chars()
    {
        if c.is_ascii_alphabetic() == true
        {
            newstr.push(c);
        }
    }

    if &newstr == "true"
    {
        return Ok(true);
    }
    else if &newstr == "false"
    {
        return Ok(false);
    }
    else
    {
        scheme_runtime_error!("Can not convert string: '{}' to boolean", newstr);
    }
}

/*pub fn bool2int(b: bool) -> i64
{
    if b == true
    {
        return 1;
    }
    else
    {
        return 0;
    };
}

pub fn bool2uint(b: bool) -> u64
{
    if b == true
    {
        return 1;
    }
    else
    {
        return 0;
    };
}*/
