use std::rc::Rc;
use std::cell::RefCell;

use crate::lexer::lexer::{LexerInfo};
use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError};
use crate::scheme_runtime_error;

use super::dynamic_serialize::SerDynValue;
use super::dynamic_common;
use super::schemesubs::{FieldVarType, FieldNameType, FieldVarArg};
use super::dynamic_scheme::{DynValue, evaluate_value, DynEnvironment};

pub fn convert_list_items(destconvarg: &FieldVarArg, 
                        srcconv: DynValue, 
                        env: Rc<RefCell<DynEnvironment>>) 
                            -> SchemeResult<SerDynValue>
{
    match srcconv
    {
        DynValue::List(list, lli) =>
        {
            let destconv = destconvarg.get_primarg()?;

            // before passing it to the serialize buffer, perfrom a conversion
            // of the symbols
            let mut evlist: Vec<SerDynValue> = Vec::with_capacity(list.len());

            for item in list
            {
                let eval = evaluate_value(&item, env.clone())?;
                let converted = convert(destconv, eval)?;

                evlist.push(converted);
            }

            return Ok(SerDynValue::List(evlist));
        },
        _ => scheme_runtime_error!("converting list items: can not convert from {} to {}", destconvarg, srcconv),
    }
}

pub fn convert(destconv: &FieldVarType, srcconv: DynValue) -> SchemeResult<SerDynValue>
{
    let convres = match srcconv
    {
        DynValue::Boolean(b, lli) => 
        {
            match *destconv
            {
                FieldVarType::String => ConvString::from(b).unwrap(),
                FieldVarType::Integer => ConvInteger::from(b).unwrap(),
                FieldVarType::Uinteger => ConvUinteger::from(b).unwrap(),
                FieldVarType::Boolean => ConvBoolean::from(b).unwrap(),
                _ => scheme_runtime_error!("convert() can't convert to {} near: {:?}", destconv, lli),
            }
        },
        DynValue::Integer(i, lli) =>
        {
            match *destconv
            {
                FieldVarType::String => ConvString::from(i).unwrap(),
                FieldVarType::Integer => ConvInteger::from(i).unwrap(),
                FieldVarType::Uinteger => ConvUinteger::from(i).unwrap(),
                FieldVarType::Boolean => ConvBoolean::from(i).unwrap(),
                _ => scheme_runtime_error!("convert() can't convert to {} near: {:?}", destconv, lli),
            }
        },
        DynValue::UInteger(u, lli) => 
        {
            match *destconv
            {
                FieldVarType::String => ConvString::from(u).unwrap(),
                FieldVarType::Integer => ConvInteger::from(u).unwrap(),
                FieldVarType::Uinteger => ConvUinteger::from(u).unwrap(),
                FieldVarType::Boolean => ConvBoolean::from(u).unwrap(),
                _ => scheme_runtime_error!("convert() can't convert to {} near: {:?}", destconv, lli),
            }
        },
        DynValue::String(s, lli) =>
        {
            match *destconv
            {
                FieldVarType::String => ConvString::from(s).unwrap(),
                FieldVarType::Integer => ConvInteger::from(s).unwrap(),
                FieldVarType::Uinteger => ConvUinteger::from(s).unwrap(),
                FieldVarType::Boolean => ConvBoolean::from(s).unwrap(),
                _ => scheme_runtime_error!("convert() can't convert to {} near: {:?}", destconv, lli),
            }
        },
        _ => scheme_runtime_error!("convert() can not convert from {} to {}", destconv, srcconv),
    };

    return Ok(convres);
}

pub struct ConvString
{
    st: SerDynValue
}

impl ConvString
{
    pub fn unwrap(self) -> SerDynValue
    {
        return self.st;
    }
}

impl From<u64> for ConvString 
{
    fn from(v: u64) -> ConvString 
    {
        return ConvString {st: SerDynValue::String(v.to_string())};
    }
}

impl From<i64> for ConvString 
{
    fn from(v: i64) -> ConvString 
    {
        return ConvString {st: SerDynValue::String(v.to_string())};
    }
}

impl From<String> for ConvString 
{
    fn from(v: String) -> ConvString 
    {
        return ConvString {st: SerDynValue::String(v)};
    }
}

impl From<bool> for ConvString 
{
    fn from(v: bool) -> ConvString 
    {
        return ConvString {st: SerDynValue::String(v.to_string())};
    }
}



pub struct ConvInteger
{
    st: SerDynValue
}

impl ConvInteger
{
    pub fn unwrap(self) -> SerDynValue
    {
        return self.st;
    }
}

impl From<u64> for ConvInteger 
{
    fn from(v: u64) -> ConvInteger 
    {
        return ConvInteger {st: SerDynValue::Integer(v as i64)};
    }
}

impl From<i64> for ConvInteger 
{
    fn from(v: i64) -> ConvInteger 
    {
        return ConvInteger {st: SerDynValue::Integer(v)};
    }
}

impl From<String> for ConvInteger 
{
    fn from(v: String) -> ConvInteger 
    {
        let i = i64::from_str_radix(&v, 10).unwrap();
        return ConvInteger {st: SerDynValue::Integer(i)};
    }
}

impl From<bool> for ConvInteger 
{
    fn from(v: bool) -> ConvInteger 
    {
        return ConvInteger {st: SerDynValue::Integer(v as i64)};
    }
}




pub struct ConvUinteger
{
    st: SerDynValue
}

impl ConvUinteger
{
    pub fn unwrap(self) -> SerDynValue
    {
        return self.st;
    }
}

impl From<u64> for ConvUinteger 
{
    fn from(v: u64) -> ConvUinteger 
    {
        return ConvUinteger {st: SerDynValue::UInteger(v)};
    }
}

impl From<i64> for ConvUinteger 
{
    fn from(v: i64) -> ConvUinteger 
    {
        return ConvUinteger {st: SerDynValue::UInteger(v as u64)};
    }
}

impl From<String> for ConvUinteger 
{
    fn from(v: String) -> ConvUinteger 
    {
        let u = u64::from_str_radix(&v, 10).unwrap();
        return ConvUinteger {st: SerDynValue::UInteger(u)};
    }
}

impl From<bool> for ConvUinteger 
{
    fn from(v: bool) -> ConvUinteger 
    {
        return ConvUinteger {st: SerDynValue::UInteger(v as u64)};
    }
}




pub struct ConvBoolean
{
    st: SerDynValue
}

impl ConvBoolean
{
    pub fn unwrap(self) -> SerDynValue
    {
        return self.st;
    }
}

impl From<u64> for ConvBoolean 
{
    fn from(v: u64) -> ConvBoolean 
    {
        let b = dynamic_common::uint2bool(v);
        return ConvBoolean {st: SerDynValue::Boolean(b)};
    }
}

impl From<i64> for ConvBoolean 
{
    fn from(v: i64) -> ConvBoolean 
    {
        let b = dynamic_common::int2bool(v);
        return ConvBoolean {st: SerDynValue::Boolean(b)};
    }
}

impl From<String> for ConvBoolean 
{
    fn from(v: String) -> ConvBoolean 
    {
        let b = dynamic_common::string2boolean(&v).unwrap();
        return ConvBoolean {st: SerDynValue::Boolean(b)};
    }
}

impl From<bool> for ConvBoolean 
{
    fn from(v: bool) -> ConvBoolean 
    {
        return ConvBoolean {st: SerDynValue::Boolean(v)};
    }
}
