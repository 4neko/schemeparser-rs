/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashSet;

use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError};
use crate::scheme_runtime_error;
use crate::lexer::lexer::LexerInfo;
use crate::common;

use super::schemesubs::{FieldVarType, ProcVarType, FieldNameType, FieldVarArg};
use super::init::{Environment, Value, evaluate_value, evaluate_expression};



#[inline]
pub fn find_dups_for_ord_num(ordnum: usize, ord_check: &mut Vec<usize>) -> SchemeResult<()>
{
    let mut i = 0;

    loop
    {
        if i < ord_check.len()
        {
            let itm = ord_check[i];
            if itm == ordnum
            {
                panic!("panic todo: duplicate {}", ordnum);
            }
            else if itm > ordnum
            {
                //insert before itm
                if i > 0
                {
                    i -= 1;
                }

                ord_check.insert(i, ordnum);
                break;
            }

            i += 1;
        }
        else
        {
            //insert in the end
            ord_check.push(ordnum);
            break;
        }
    }

    return Ok(());
}

#[derive(PartialEq)]
pub enum Contains
{
    String, Integer, UInteger, Boolean, Datatype
}

pub struct CommonReader<'cr>
{
    env: Rc<RefCell<Environment>>,
    li: &'cr LexerInfo,
    args: &'cr [Value],
    counter: usize,
}

impl<'cr> CommonReader<'cr>
{
    #[inline]
    fn resolve_symbol(&self, name: &String, li: &LexerInfo) -> SchemeResult<Value>
    {
        match self.env.borrow().get(name) 
        {
            Some(r) => return Ok(r),
            None => scheme_runtime_error!("Indentifier: '{}' not found, near: {}",
                                            name, li),
        }
    }

    pub fn new(env: Rc<RefCell<Environment>>, li: &'cr LexerInfo, args: &'cr [Value], counter: usize) -> Self
    {
        return Self {env: env.clone(), li: li, args: args, counter: counter};
    }

    pub fn is_eof(&self) -> bool
    {
        return self.counter >= self.args.len();
    }

    pub fn get_counter(&self) -> usize
    {
        return self.counter;
    }

    pub fn get_prev_lexerinfo(&self) -> LexerInfo
    {
        let index = if self.counter == 0
        {
            self.counter
        }
        else
        {
            self.counter - 1
        };

        return self.args[index].extract_lexerinfo();
    }

    pub fn left(&self) -> usize
    {
        return self.args.len() - self.counter;
    }

    pub fn eof(&self) -> SchemeResult<()>
    {
        if self.counter < self.args.len()
        {
            scheme_runtime_error!("EOF was not reached {} < {}, near: {}",
                                    self.counter,
                                    self.args.len(),
                                    self.li);
        }

        return Ok(());
    }

    pub fn evaluate_list<F>(&mut self, throw: bool, steps: usize, mut hndl: F) -> SchemeResult<()>
    where F: FnMut(&Vec<Value>, Rc<RefCell<Environment>>, &LexerInfo) -> SchemeResult<()>
    {
        let l_steps = if steps == 0
        {
            self.args.len()
        }
        else
        {
            if (self.counter + steps) > self.args.len()
            {
                panic!("todo: step too large {} len: {}, \n {:?}", steps, self.args.len(), &self.args[self.counter..self.args.len()]);
            }
            else
            {
                self.counter + steps
            }
        };

        let mut offset = 0;
        for arg in &self.args[self.counter..l_steps]
        {
            match arg
            {
                Value::List(ref list, ref li) => 
                {
                    if list.len() > 0 
                    {
                        hndl(list, self.env.clone(), li)?;
                        offset += 1;
                    }
                    else
                    {
                        panic!("errrrr");
                    }
                },
                _ =>
                {
                    if throw == true
                    {
                        scheme_runtime_error!("Expected procedures, got: '{}', near: {}", 
                                                arg, arg.extract_lexerinfo());
                    }
                }
            }
        }

        self.counter += offset;

        return Ok(());
    
    }

    pub fn evaluate_manual<F>(&mut self, steps: usize, mut hndl: F) -> SchemeResult<()>
    where F: FnMut(&Value, Rc<RefCell<Environment>>) -> SchemeResult<()>
    {
        let l_steps = if steps == 0
        {
            self.args.len()
        }
        else
        {
            if (self.counter + steps) > self.args.len()
            {
                panic!("todo: step too large {}", steps);
            }
            else
            {
                self.counter + steps
            }
        };

        let mut offset = 0;
        for arg in &self.args[self.counter..l_steps]
        {
            hndl(arg, self.env.clone())?;
            offset += 1;
        }

        self.counter += offset;

        return Ok(());
    
    }

    pub fn common_read_dt(&mut self) -> SchemeResult<FieldVarArg>
    {
        if self.is_eof() == true
        {
            scheme_runtime_error!("Unexpected EOF while attepting to read 'data type symbol', near: {}", self.li);
        }

        let ref val = self.args[self.counter];
        match val
        {
            Value::List(ref list, ref li) => 
            {
                let eval = evaluate_expression(list, self.env.clone(), li)?;
                
                let dt = match eval
                {
                    Value::RetDt(dt) => dt,
                    _ => panic!("todo: err 010101"),
                };

                //next
                self.counter += 1;

                return Ok(dt);
            },
            _ => scheme_runtime_error!("Expected specific procedure (dt), found: '{}', near: {}",
                                        val, val.extract_lexerinfo()),
        }
    }

    pub fn common_read_fieldenum(&mut self) -> SchemeResult<FieldVarType>
    {
        if self.is_eof() == true
        {
            scheme_runtime_error!("Unexpected EOF while attepting to read 'field enum', near: {}", self.li);
        }

        let ref arg = self.args[self.counter];
        match arg
        {
            Value::Symbol(ref name, ref li) => 
            {
                let symb = self.resolve_symbol(name, li)?;

                match symb
                {
                    Value::FieldVarEnum(s) => 
                    {
                        //increase counter
                        self.counter += 1;
                        
                        //return result
                        return Ok(s.clone());
                    }
                    _ => scheme_runtime_error!("Expected speficif keyword 'Field data type' \
                                                i.e f/list, found: '{}', near {}",
                                                symb, symb.extract_lexerinfo()),
                }
            },
            _ => scheme_runtime_error!("Expected speficif keyword 'Field data type' \
                                        i.e f/list, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        }
    }

    pub fn common_read_field_name(&mut self) -> SchemeResult<FieldNameType>
    {
        if self.is_eof() == true
        {
            scheme_runtime_error!("Unexpected EOF while attepting to read 'field name or symbol', near: {}", self.li);
        }

        let ref arg = self.args[self.counter];

        match arg
        {
            Value::String(ref s, ref li) => 
            {
                let temp = self.read_string(s, li)?;
                self.counter += 1;

                return Ok(FieldNameType::Name(temp));
            },
            Value::Symbol(ref name, ref li) => 
            {
                let symb = self.resolve_symbol(name, li)?;

                match symb
                {
                    Value::FieldNameEnum(s) => 
                    {
                        //increase counter
                        self.counter += 1;
                        
                        //return result
                        return Ok(s.clone());
                    }
                    _ => scheme_runtime_error!("Expected speficif keyword 'Field data type' \
                                                i.e f/list, found: '{}', near {}",
                                                symb, symb.extract_lexerinfo()),
                }
            },
            _ => scheme_runtime_error!("Expected speficif keyword 'Field data type' \
                                        i.e f/list or <String>, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        }
    }

    pub fn common_read_expression(&mut self) -> SchemeResult<Value>
    {
        if self.is_eof() == true
        {
            scheme_runtime_error!("Unexpected EOF while attepting to read 'expression', near: {}", self.li);
        }

        let ref val = self.args[self.counter];
        match val
        {
            Value::List(ref list, ref li) => 
            {
                let eval = evaluate_expression(list, self.env.clone(), li)?;
                
                //next
                self.counter += 1;

                return Ok(eval);
            },
            _ => scheme_runtime_error!("Failed to read expression, got: '{}', near: {}",
                                        val, val.extract_lexerinfo()),
        }
    }




    pub fn common_read_varenum(&mut self) -> SchemeResult<ProcVarType>
    {
        if self.is_eof() == true
        {
            scheme_runtime_error!("Unexpected EOF while attepting to read 'varenum', near: {}", self.li);
        }

        let ref arg = self.args[self.counter];

        match arg
        {
            Value::Symbol(ref name, ref li) => 
            {
                let symb = self.resolve_symbol(name, li)?;
                match symb
                {
                    Value::ProcVarEnum(s) => 
                    {
                        //increase counter
                        self.counter += 1;
                        
                        //return result
                        return Ok(s.clone());
                    }
                    _ => scheme_runtime_error!("Expected speficif keyword 'Var enum' \
                                                i.e string, found: '{}', near {}",
                                                symb, symb.extract_lexerinfo()),
                }
            },
            _ => scheme_runtime_error!("Expected speficif keyword 'Var enum' \
                                        i.e string, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        }
    }

    pub fn common_read_symbol(&mut self) -> SchemeResult<Value>
    {
        let ref arg = self.args[self.counter];

        match arg
        {
            Value::Symbol(ref name, ref li) => 
            {
                let symb = self.resolve_symbol(name, li)?;
                
                //increase counter
                self.counter += 1;

                return Ok(symb);
            },
            _ => scheme_runtime_error!("Expected speficif keyword type, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        }
    }

    pub fn may_read_symbol(&mut self) -> SchemeResult<Option<Value>>
    {
        match self.args[self.counter]
        {
            Value::Symbol(ref name, ref li) => 
            {
                let symb = self.resolve_symbol(name, li)?;

                //increase counter
                self.counter += 1;

                return Ok(Some(symb.clone()));
            },
            _ => return Ok(None)
        }
    }

    pub fn read_string(&mut self, s: &String, li: &LexerInfo) -> SchemeResult<String>
    {
        if common::contains_printable_all(s) == false
        {
            scheme_runtime_error!("Empty string near: {}", li);
        }

        return Ok(s.clone());
    }

    pub fn common_contains_next(&self, c: Contains, offset: usize) -> bool
    {
        if self.is_eof() == true
        {
            return false;
        }

        let off = self.counter + offset;
        if off >= self.args.len()
        {
            return false;
        }

        match self.args[off]
        {
            Value::String(_, _) => 
            {
                return c == Contains::String;
            },
            Value::UInteger(_, _) =>
            {
                return c == Contains::UInteger;
            },
            Value::Integer(_, _) =>
            {
                return c == Contains::Integer;
            },
            Value::Boolean(_, _) =>
            {
                return c == Contains::Boolean;
            },
            Value::Symbol(_, _) =>
            {
                return c == Contains::Datatype;
            }
            _ => return false,
        }
    }

    pub fn common_read_string(&mut self) -> SchemeResult<String>
    {
        if self.is_eof() == true
        {
            scheme_runtime_error!("Unexpected EOF while attepting to read String, near: {}", self.li);
        }

        let ref arg = self.args[self.counter];

        match arg
        {
            Value::String(ref s, ref li) => 
            {
                let res = self.read_string(s, li)?;

                //increase counter
                self.counter += 1;

                return Ok(res);
            },
            _ => scheme_runtime_error!("Expected <String>, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        };
    }

    pub fn common_read_int(&mut self) -> SchemeResult<i64>
    {
        let ref arg = self.args[self.counter];

        match arg
        {
            Value::Integer(ref i, _) => 
            {
                //increase counter
                self.counter += 1;

                return Ok(*i);
            },
            _ => scheme_runtime_error!("Expected <Integer>, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        };   
    }

    pub fn common_read_uint(&mut self) -> SchemeResult<u64>
    {
        let ref arg = self.args[self.counter];

        match arg
        {
            Value::UInteger(ref u, _) => 
            {
                //increase counter
                self.counter += 1;

                return Ok(*u);
            },
            _ => scheme_runtime_error!("Expected <UInteger>, found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        };   
    }


    pub fn common_read_sib(&mut self) -> SchemeResult<Value>
    {
        let arg = &self.args[self.counter];
        match arg
        {
            Value::String(s, ref li) =>
            {
                if common::contains_printable_all(s) == false
                {
                    scheme_runtime_error!("Empty string near: {}", li);
                }

                //increase counter
                self.counter += 1;
    
                return Ok(arg.clone());
            },
            Value::Integer(_, _) | Value::UInteger(_, _) | Value::Boolean(_, _) =>
            {
                //increase counter
                self.counter += 1;

                return Ok(arg.clone());
            }
            _ => scheme_runtime_error!("Expected <String|Integer|Uinteger|Boolean> \
                                        found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        }
    }

    pub fn common_read_bool(&mut self) -> SchemeResult<bool>
    {
        let ref arg = self.args[self.counter];

        match arg
        {
            Value::Boolean(ref b, _) => 
            {
                //increase counter
                self.counter += 1;

                return Ok(*b);
            },
            _ => scheme_runtime_error!("Expected <Boolean> \
                                        found: '{}', near {}",
                                        arg, arg.extract_lexerinfo()),
        }
    }
    
    pub fn common_read_hset_string_nodup(&mut self) -> SchemeResult<HashSet<String>>
    {
        let arg = &self.args[self.counter];
        let argeval = evaluate_value(arg, self.env.clone())?;
        let mut retlist: HashSet<String> = HashSet::new();

        //let vardata = 
        match argeval
        {
            Value::List(list, ref li) => 
            {  
                for l in list
                {
                    match l
                    {
                        Value::String(s, lli) =>
                        {
                            if common::contains_printable_all(&s) == false
                            {
                                scheme_runtime_error!("Empty string near: {}", li);
                            }

                            retlist.insert(s);
                        },
                        _ => 
                        scheme_runtime_error!("Expected <String>, found: \
                                                '{}', near {}",
                                                l, l.extract_lexerinfo()),

                    }
                }
            },
            _ => scheme_runtime_error!("Expected <String>, found: \
                                                '{}', near {}",
                                                argeval, argeval.extract_lexerinfo()),
        }
        

        self.counter += 1;

        return Ok(retlist);
    }


}
