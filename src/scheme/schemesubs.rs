
/* 
 * schemeparser - scheme config serialization configurator
 * Copyright (C) 2021 Aleksandr Morozov, RELKOM s.r.o
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt;
use std::rc::Rc;
use std::collections::HashSet;

use linked_hash_map::LinkedHashMap;
use bit_field::BitField;

use crate::runtime_err::{SchemeResult, SchemeError, SchemeRuntimeError};
use crate::scheme_runtime_error;
use crate::lexer::lexer::LexerInfo;

#[derive(PartialEq, Clone, Eq, Hash, Debug)]
pub enum FieldVarType
{
    String,
    Uinteger,
    Integer,
    Boolean,
    List,
    HashMap,
    Enum,
    Struct,
    Field,
}

impl FieldVarType
{
    pub fn is_primitive(&self) -> bool
    {
        match *self
        {
            Self::String |
            Self::Integer|
            Self::Uinteger|
            Self::Boolean => return true,
            _ => return false
        }
    }
}

impl fmt::Display for FieldVarType 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Self::String => write!(f, "String"),
            Self::Uinteger => write!(f, "Uinteger"),
            Self::Integer => write!(f, "Integer"),
            Self::Boolean => write!(f, "Boolean"),
            Self::List => write!(f, "List"),
            Self::HashMap => write!(f, "HashMap"),
            Self::Enum => write!(f, "Enum"),
            Self::Struct => write!(f, "Struct"),
            Self::Field => write!(f, "Field"),
        }
    }
}

#[derive(PartialEq, Clone, Eq, Hash, Debug)]
pub enum FieldNameType
{
    Anon,
    Name(String)
}

impl fmt::Display for FieldNameType 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Self::Anon => write!(f, "anonymous"),
            Self::Name(ref s) => write!(f, "{}", s),
        }
    }
}

impl FieldNameType
{
    pub fn is_anon(&self) -> bool
    {
        match *self
        {
            Self::Anon => return true,
            Self::Name(_) => return false,
        }
    }

    pub fn clone_fieldname(&self) -> String
    {
        match *self
        {
            Self::Anon => panic!("assertion: clone_fieldname called when self is type: Anon"),
            Self::Name(ref n) => return n.clone()
        }
    }

    pub fn get_fieldname(&self) -> &String
    {
        match *self
        {
            Self::Anon => panic!("assertion: clone_fieldname called when self is type: Anon"),
            Self::Name(ref n) => return n
        }
    }
}

#[derive(PartialEq, Clone, Eq, Hash, Debug)]
pub enum FieldVarArg
{
    Enum(LinkedHashMap<String, String>),
    List(Box<FieldVarArg>),
    HashMap(String, Box<FieldVarArg>),
    Struct,
    Field(String),
    PrimArg(FieldVarType)
}

impl FieldVarArg
{
    /// A defined Field() for the List without foregn field
    pub const FIELD_NONE: &'static str = "!NONE!";

    pub fn is_primitive(&self) -> bool
    {
        match *self
        {
            Self::PrimArg(_) => return true,
            Self::List(ref l) => return l.is_primitive(),
            _ => return false,
        }
    }

    pub fn get_primarg(&self) -> SchemeResult<&FieldVarType>
    {
        match *self
        {
            Self::PrimArg(ref r) => return Ok(r),
            _ => panic!("assertion: FieldVarArg::get_primarg, cant return PrimArg from {}", self),
        }
    }
}

impl fmt::Display for FieldVarArg 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            Self::Enum(ref _map) => write!(f, "Enum"),
            Self::List(ref _box_fva) => write!(f, "List"),
            Self::HashMap(ref _key, ref _val) => write!(f, "HashMap"),
            Self::Struct => write!(f, "Struct"),
            Self::Field(ref _fieldname) => write!(f, "Field"),
            Self::PrimArg(ref _fva) => write!(f, "PrimArg"),
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct SerializeAs
{
    order: usize,
    fieldname: FieldNameType,
    fieldas: FieldVarArg
}

impl fmt::Display for SerializeAs 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        write!(f, "order: {}, fieldname: {}", self.order, self.fieldname)
    }
}

impl SerializeAs
{
    pub fn new(order: usize,
                fieldname: FieldNameType,
                fieldas: FieldVarArg) -> Self
    {
        return Self{
                    order: order, 
                    fieldname: fieldname, 
                    fieldas: fieldas
                };
    }

    pub fn new_dummy() -> Self
    {
        return Self 
            {
                order: 0,
                fieldname: FieldNameType::Anon,
                fieldas: FieldVarArg::Struct
            };
    }

    pub fn get_to_datatype(&self) -> &FieldVarArg
    {
        return &self.fieldas;
    }

    pub fn can_use_for_arg(&self) -> bool
    {
        return (self.fieldas.is_primitive() == true) &&
                (self.fieldname.is_anon() == false);
    }

    pub fn is_correct_for_proc(&self, li: &LexerInfo) -> SchemeResult<()>
    {
        match self.fieldas
        {
            FieldVarArg::PrimArg(_) => 
                scheme_runtime_error!("(proc ... (dt ...)) in proc: '{}' can not use primitive serialization datatype, near: {}", self.fieldname, li),
            FieldVarArg::List(ref l) =>
            {
                if l.is_primitive() == true
                {
                    scheme_runtime_error!("(proc ... (dt f/list)) in proc: '{}', can not use primitive with list, near: {}", self.fieldname, li);
                }
            }
            FieldVarArg::HashMap(ref k, ref v) =>
            {
                if k.is_empty() == true
                {
                    scheme_runtime_error!("(proc ... (dt f/hashmap ...) in proc: '{}', foreign key is empty, near: {}", self.fieldname, li);
                }

                if v.is_primitive() == true
                {
                    scheme_runtime_error!("(proc ... (dt f/hashmap \"{}\" ...)) in proc: '{}', value can not be primitive, near: {}", k, self.fieldname, li);
                }
            },
            _ => {}
        }

        return Ok(());
    }

    pub fn is_anonymous(&self) -> bool
    {
        return self.fieldname.is_anon();
    }

    pub fn clone_field_name(&self) -> String
    {
        return self.fieldname.clone_fieldname();
    }

    pub fn get_field_title(&self) -> &String
    {
        return self.fieldname.get_fieldname();
    }

    pub fn get_field_ord(&self) -> usize
    {
        return self.order;
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct SchemeArgFlags
{
    flags: u8,
    //restricted: Option<HashSet<String>>,
}

impl SchemeArgFlags
{
    const F_REPEAT: usize = 0;
    const F_COMBINE: usize = 1;
    const F_OPTIONAL: usize = 2;
    const F_NOSTRUCT: usize = 3;
    const F_PACKHM: usize = 4;

    pub fn new() -> Self
    {
        return Self{flags: 0};//, restricted: None};
    }

/*    pub fn set_restriction(&mut self, procnames: HashSet<String>) -> SchemeResult<()>
    {
        if self.restricted.is_some() == true
        {
            panic!("todo: duplicate restrcitions");
        }

        self.restricted = Some(procnames);

        return Ok(());
    }

    pub fn get_restrictions(&self) -> Option<std::collections::hash_set::Iter<'_, String>>
    {
        return match self.restricted
        {
            Some(ref r) => Some(r.iter()),
            None => None
        };
    }

    pub fn is_in_restriction(&self, procname: &String) -> bool
    {
        if self.restricted.is_none() == true
        {
            return true;
        }
        else
        {
            return self.restricted.as_ref().unwrap().contains(procname);
        }
    }*/

    pub fn set_repeat(&mut self)
    {
        self.flags.set_bit(SchemeArgFlags::F_REPEAT, true);
    }

    pub fn set_combile(&mut self)
    {
        self.flags.set_bit(SchemeArgFlags::F_COMBINE, true);
    }

    pub fn set_optional(&mut self)
    {
        self.flags.set_bit(SchemeArgFlags::F_OPTIONAL, true);
    }

    pub fn set_nostruct(&mut self)
    {
        self.flags.set_bit(SchemeArgFlags::F_NOSTRUCT, true);
    }

    pub fn is_repeatable(&self) -> bool
    {
        return self.flags.get_bit(SchemeArgFlags::F_REPEAT);
    }

    pub fn is_combine(&self) -> bool
    {
        return self.flags.get_bit(SchemeArgFlags::F_COMBINE);
    }

    pub fn is_optional(&self) -> bool
    {
        return self.flags.get_bit(SchemeArgFlags::F_OPTIONAL);
    }

    pub fn is_nostruct(&self) -> bool
    {
        return self.flags.get_bit(SchemeArgFlags::F_NOSTRUCT);
    }

    pub fn is_packtohashmap(&self) -> bool
    {
        return self.flags.get_bit(SchemeArgFlags::F_PACKHM);
    }

}

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum ProcVarTypeOpts
{
    None,
    Vector(ProcVarType),
}

impl fmt::Debug for ProcVarTypeOpts
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        match *self
        {
            ProcVarTypeOpts::None => write!(f, "ProcVarTypeOpts::None"),
            ProcVarTypeOpts::Vector(_) => write!(f, "ProcVarTypeOpts::Vector"),
        }
    }
}

impl ProcVarTypeOpts
{
    pub fn is_none(&self) -> bool
    {
        return *self == ProcVarTypeOpts::None;
    }

    pub fn get_vector_pvt(&self) -> &ProcVarType
    {
        match *self
        {
            ProcVarTypeOpts::Vector(ref p) => return p,
            _ => panic!("assertion: Misuse get_vector_pvt() works only with Vector, has {:?}", self),
        }
    }
}

#[derive(PartialEq, Clone, Eq, Hash)]
pub enum ProcVarType
{
    String,
    Uinteger,
    Integer,
    Boolean,
    StrSymbol,
    IntSymbol,
    Vector,
}

impl fmt::Display for ProcVarType 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            ProcVarType::String => write!(f, "#<String>"),
            ProcVarType::Integer => write!(f, "#<Integer>"),
            ProcVarType::Uinteger => write!(f, "#<Uinteger>"),
            ProcVarType::Boolean => write!(f, "#<Boolean>"),
            ProcVarType::StrSymbol => write!(f, "#<Symbol(string)>"),
            ProcVarType::IntSymbol => write!(f, "#<Symbol(integer)>"),
            ProcVarType::Vector => write!(f, "#<Vector>"),
        }
    }
}

impl fmt::Debug for ProcVarType 
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        match *self 
        {
            ProcVarType::String => write!(f, "#<String>"),
            ProcVarType::Integer => write!(f, "#<Integer>"),
            ProcVarType::Uinteger => write!(f, "#<Uinteger>"),
            ProcVarType::Boolean => write!(f, "#<Boolean>"),
            ProcVarType::StrSymbol => write!(f, "#<Symbol(string)>"),
            ProcVarType::IntSymbol => write!(f, "#<Symbol(integer)>"),
            ProcVarType::Vector => write!(f, "#<Vector>"),
        }
    }
}

#[derive(Clone, PartialEq)]
pub struct ProcVar
{
    vartype: ProcVarType,
    optvartype: ProcVarTypeOpts,
    serialz: SerializeAs,
}

impl ProcVar
{
    pub fn new(vartype: ProcVarType, optvartype: ProcVarTypeOpts, serialz: SerializeAs) -> Self
    {
        return ProcVar{vartype, optvartype, serialz};
    }

    pub fn get_vartype(&self) -> &ProcVarType
    {
        return &self.vartype;
    }

    pub fn get_vartype_opts(&self) -> &ProcVarTypeOpts
    {
        return &self.optvartype;
    }

    pub fn get_seialz(&self) -> &SerializeAs
    {
        return &self.serialz;
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct SchemeProc
{
    procs: HashSet<String>,
    flags: SchemeArgFlags,
    serialz: SerializeAs
}

impl SchemeProc
{
    pub fn new(procs: HashSet<String>, flags: SchemeArgFlags, serialz: SerializeAs) -> Self
    {
        return Self {procs: procs, flags: flags, serialz: serialz};
    }

    pub fn get_expected_procs(&self) -> String
    {
        let recs: Vec<String> = self.procs.iter().map(|v| v.clone()).collect();
        
        return recs.join(" ");
    }

    pub fn get_proc_serialz(&self) -> &SerializeAs
    {
        return &self.serialz;
    }

    pub fn iter_procs_titles(&self) -> std::collections::hash_set::Iter<'_, String>
    {
        return self.procs.iter();
    }

    pub fn contains(&self, procname: &String) -> bool
    {
        return self.procs.contains(procname);
    }

    pub fn contains_sp(&self, sp: &SchemeProc) -> Option<String>
    {
        if self.procs.len() >= sp.procs.len()
        {
            for p in self.procs.iter()
            {
                if sp.procs.contains(p) == true
                {
                    return Some(p.clone());
                }
            }
        }
        else
        {
            for p in sp.procs.iter()
            {
                if self.procs.contains(p) == true
                {
                    return Some(p.clone());
                }
            }
        }   

        return None;
    }

    pub fn contains_full_match(&self, sp: &SchemeProc) -> bool
    {
        let mut m: usize = 0;

        if self.procs.len() != sp.procs.len()
        {
            // length is different
            return false;
        }

        for p in sp.procs.iter()
        {
            if self.procs.contains(p) == true
            {
                m += 1;
            }
        }

        if m != self.procs.len()
        {
            return false;
        }

        return true;
    }

    pub fn get_proc_flags(&self) -> &SchemeArgFlags
    {
        return &self.flags;
    }
}

impl fmt::Display for SchemeProc
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result 
    {
        let recs: Vec<String> = 
            self.procs.iter().map(|v| v.clone()).collect();

        write!(f, "{}", recs.join(" "))
    }
}

#[derive(Clone, PartialEq)]
pub struct SchemeProcedure
{
    title: String,
    empty: bool,
    args: LinkedHashMap<String, ProcVar>,
    procs: Vec<SchemeProc>, //title, flags
}

impl SchemeProcedure
{
    pub fn new(title: String, empty: bool) -> Self
    {
        return SchemeProcedure
            {
                title: title, 
                empty: empty, 
                args: LinkedHashMap::new(), 
                procs: Vec::new()
            };
    }

    pub fn is_set_empty(&self) -> bool
    {
        return self.empty;
    }

    pub fn get_total_fields(&self) -> usize
    {
        return self.args.len() + self.procs.len();
    }

    pub fn is_empty(&self) -> bool
    {
        if self.args.is_empty() == true && self.procs.is_empty() == true
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    pub fn clone_title(&self) -> String
    {
        return self.title.clone();
    }

    pub fn proc_search_match(&self, titles: &HashSet<String>) -> Option<String>
    {
        for p in self.procs.iter()
        {
            for t in titles
            {
                if p.contains(t) == true
                {
                    return Some(t.clone());
                }
            }
        }

        return None;
    }

    /*pub fn get_proc_flags(&self, proctitle: &String) -> Option<&SchemeArgFlags>
    {
        for p in self.procs.iter()
        {
            if p.contains(proctitle) == true
            {
                return Some(p.get_proc_flags());
            }
        }

        return None;
    }*/

    pub fn get_proc(&self, proctitle: &String) -> Option<&SchemeProc>
    {
        for p in self.procs.iter()
        {
            //println!("{:?}", p);
            if p.contains(proctitle) == true
            {
                return Some(p);
            }
        }

        return None;
    }

    pub fn get_expected_procs(&self) -> String
    {
        let recs: Vec<String> = self.procs.iter().map(|v| v.get_expected_procs()).collect();
        
        return recs.join(" ");
    }

    pub fn get_title(&self) -> &String
    {
        return &self.title;
    }

    pub fn get_args(&self) -> linked_hash_map::Iter<'_, String, ProcVar>
    {
        return self.args.iter();
    }

    pub fn iter_procs(&self) ->std::slice::Iter<'_, SchemeProc>
    {
        return self.procs.iter();
    }

    pub fn get_args_len(&self) -> usize
    {
        return self.args.len();
    }

    pub fn insert_arg(&mut self, varname: String, vt: ProcVar) -> bool
    {
        if self.args.contains_key(&varname) == true
        {
            return false;
        }

        self.args.insert(varname, vt);

        return true;
    }

    pub fn insert_procs(&mut self, sp: SchemeProc) -> Option<String>
    {
        for ssp in self.procs.iter()
        {
            if let Some(dp) = ssp.contains_sp(&sp)
            {
                return Some(dp.clone());
            }
        }
        
        self.procs.push(sp);

        return None;
    }
}

